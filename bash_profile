if [ -f ~/.bashrc ]; then
   source ~/.bashrc
fi

if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
    # SWAY
    export XKB_DEFAULT_OPTIONS=caps:shift
    # WAYLAND
    export XDG_SESSION_TYPE=wayland
    export XDG_CURRENT_DESKTOP=sway
    #export GDK_BACKEND=wayland
    #export CLUTTER_BACKEND=wayland
    #export ECORE_EVAS_ENGINE=wayland_egl
    #export ELM_ACCEL=gl
    #export ELM_DISPLAY=wl
    #export SDL_VIDEODRIVER=wayland
    export _JAVA_AWT_WM_NONREPARENTING=1
    export QT_WAYLAND_DISABLE_WINDOWDECORATION=0
    export QT_QPA_PLATFORM=wayland
    #export QT_WAYLAND_FORCE_DPI=physical
	#export QT_QPA_PLATFORMTHEME="qt5ct"
    #export MOZ_ENABLE_WAYLAND=1
    #export MOZ_DBUS_REMOTE=1
    #export IBUS_ENABLE_SYNC_MODE=1
fi


# ENV VARS
set -U PAGER more
set -U EDITOR nvim
set -U BROWSER firefox

set -g -x GTK_IM_MODULE cedilla
set -g -x QT_IM_MODULE cedilla

#set -g -x TERM xterm-256color
set -g -x TERM alacritty

# TOOLS
set -g -x ANDROID_HOME $HOME/.usr/share/android/Sdk
set -g -x GOPATH $HOME/Documents/devel/projects/golang-workspace
set -g -x JAVA_HOME /usr/lib/jvm/java-11-openjdk/
set -g -x STUDIO_JDK /usr/lib/jvm/java-11-openjdk/

# PATH
set -l RUST $HOME/.cargo/bin
set -l SCRIPTS $HOME/.dotfiles/scripts
set -l ANDROID_SDK $HOME/.usr/share/android/Sdk/platform-tools
set -l GO_BIN $GOPATH/bin
set -l HOME_BIN $HOME/.usr/bin

set -g -x PATH $PATH $RUST $SCRIPTS $ANDROID_SDK $JAVA_HOME $GO_BIN $HOME_BIN

# XDG
set -g -x XDG_DATA_DIRS $XDG_DATA_DIRS:"/var/lib/flatpak/exports/share":"$HOME/.local/share/flatpak/exports/share"

# NNN
set -g -x COLORTERM '24bit'
set -g -x NNN_FIFO /tmp/nnn.fifo
set -g -x NNN_BMS 'd:~/Documents;D:~/Downloads/'
set -g -x NNN_COLORS "2136" # use a different color for each context
set -g -x NNN_FALLBACK_OPENER xdg-open
set -g -x NNN_FIFO "/tmp/nnn.fifo"
set -g -x SPLIT "v"

# SWAY
set -g -x WLR_NO_HARDWARE_CURSORS 1
set -g -x XKB_DEFAULT_OPTIONS caps:shift
set -g -x _JAVA_AWT_WM_NONREPARENTING 1
set -g -x QT_WAYLAND_DISABLE_WINDOWDECORATION 1
set -g -x XDG_CURRENT_DESKTOP sway

# WAYLAND
set -g -x XDG_SESSION_TYPE wayland
set -g -x QT_QPA_PLATFORM "wayland;xcb"
set -g -x QT_QPA_PLATFORMTHEME qt5ct
set -g -x GDK_BACKEND wayland
set -g -x CLUTTER_BACKEND wayland
set -g -x SDL_VIDEODRIVER wayland
set -g -x MOZ_ENABLE_WAYLAND 1

# SSH AGENT
set -e SSH_AGENT_PID
if not set -q gnupg_SSH_AUTH_SOCK_by or test $gnupg_SSH_AUTH_SOCK_by -ne $fish_pid
#    set -gx SSH_AUTH_SOCK (gpgconf --list-dirs agent-ssh-socket)
end

# COLORS
set -x LS_COLORS "$LS_COLORS:ow=1;7;34:st=30;44:su=30;41"

# FISH
set fish_greeting					       ''
set fish_color_autosuggestion              normal
set fish_color_cancel                      -r
set fish_color_command                     --bold
set fish_color_comment                     brmagenta
set fish_color_cwd                         green
set fish_color_cwd_root                    red
set fish_color_end                         brmagenta
set fish_color_error                       brred
set fish_color_escape                      brcyan
set fish_color_history_current             --bold
set fish_color_host                        normal
set fish_color_match                       --background=brblue
set fish_color_normal                      normal
set fish_color_operator                    cyan
set fish_color_param                       brblue
set fish_color_quote                       yellow
set fish_color_redirection                 bryellow
set fish_color_search_match                'bryellow' '--background=brblack'
set fish_color_selection                   'white' '--bold' '--background=brblack'
set fish_color_status                      red
set fish_color_user                        brgreen
set fish_color_valid_path                  --underline
set fish_pager_color_prefix                normal
set fish_pager_color_completion            normal
set fish_pager_color_description           yellow
set fish_pager_color_selected_background   -r

# FUNCTIONS
function maintenance_clean
    echo "Clean Pacman Cache"
    sudo pacman -Sc
    echo "Clean Systemd Temp Files"
    sudo systemd-tmpfiles --clean
end

function flac2mp3
    for file in ./*.flac; do
        ffmpeg -i "$file" -qscale:a 0 "$file.mp3"
    end
end

function weather_krakow
    curl --compressed "wttr.in/krakow"
end

function weather_campinas
    curl --compressed "wttr.in/campinas"
end

function weather_campos
    curl --compressed "wttr.in/campos_do_jordao"
end

function weather_everest
    curl --compressed "wttr.in/~Everest"
end

function moon
    curl --compressed "wttr.in/moon"
end

function myexternalip
    curl --compressed "myexternalip.com/raw"
end

function genpass
    base64 /dev/urandom | head -c21
end

function genpass_clipboard
    base64 /dev/urandom | head -c21 | wl-copy
end

function mount_frambo
    sshfs thiago@frambo:/ext-disk ~/Documents/mount/frambo
end


# ALIAS
alias ls='exa --group --git --group-directories-first'
alias tree='exa --tree --level=3'
alias df "duf"

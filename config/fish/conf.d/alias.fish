# ALIAS
alias swaywm "cd ~ ; sway -V 2> ~/.cache/sway.log"
alias nnn "nnn -d -Q -D"

alias maintenance_lostfiles 'sudo lostfiles'
alias maintenance_foreign 'yay -Qm'
alias maintenance_failed 'systemctl --failed'
alias maintenance_errors 'journalctl -p 3 -xb'
alias maintenance_orphans 'sudo pacman -Rns (pacman -Qtdq)'
alias maintenance_broken_links 'sudo find / -xtype l -print'
alias maintenance_clean_deps 'yay -Yc'
alias maintenance_last_30 "expac --timefmt='%Y-%m-%d %T' '%l\t%n' | sort | tail -n 30"
alias maintenance_stats "yay -P --stats"
alias maintenance_services "systemctl list-units --type=service"
alias maintenance_pkg_history "expac --timefmt='%Y-%m-%d %T' '%l\t%n'|sort -n"
alias maintenance_startup "systemd-analyze plot > ~/output.svg"
alias maintenance_pacnew "find /etc -regextype posix-extended -regex \".+\.pac(new|save)\" 2> /dev/null"

alias _projects "cd ~/Documents/devel/projects"
alias _go "cd ~/Documents/devel/projects/golang-workspace/src"
alias _rainbow "cd ~/Documents/devel/projects/golang-workspace/src/gitlab.com/rainbowproject"
alias _rust "cd ~/Documents/devel/projects/rust-workspace"
alias _opensource "cd ~/Documents/devel/opensource"

# Options
set __fish_git_prompt_show_informative_status
set __fish_git_prompt_showcolorhints
set __fish_git_prompt_showupstream "informative"

# Colors
set green (set_color green)
set magenta (set_color magenta)
set normal (set_color normal)
set red (set_color red)
set yellow (set_color yellow)

set __fish_git_prompt_color_branch magenta --bold
set __fish_git_prompt_color_dirtystate white
set __fish_git_prompt_color_invalidstate red
set __fish_git_prompt_color_merging yellow
set __fish_git_prompt_color_stagedstate yellow
set __fish_git_prompt_color_upstream_ahead green
set __fish_git_prompt_color_upstream_behind red


# Icons
set __fish_git_prompt_char_cleanstate '👍 '
set __fish_git_prompt_char_conflictedstate '⚠️ '
set __fish_git_prompt_char_dirtystate '💩 '
set __fish_git_prompt_char_invalidstate '🤮 '
set __fish_git_prompt_char_stagedstate '🚥 '
set __fish_git_prompt_char_stashstate '📦 '
set __fish_git_prompt_char_stateseparator ' | '
set __fish_git_prompt_char_untrackedfiles '🔍 '
set __fish_git_prompt_char_upstream_ahead '☝️ '
set __fish_git_prompt_char_upstream_behind '👇 '
set __fish_git_prompt_char_upstream_diverged '🚧 '
set __fish_git_prompt_char_upstream_equal '💯 '

set __icons '🧟' '🚴' '👿' '👻' '🐒' '🐃' '🐄' '🦖' '🐧' '🦃' '🐓' '🐡'

function fish_prompt
    set idx (random 1 12)
    set -l prefix $__icons[$idx]💨

    if test -n "$SSH_TTY"
        echo -n (set_color red)"$USER"(set_color white)'@'(set_color yellow)(prompt_hostname)' '
    end

    set_color -o

    if test "$USER" = 'root'
        echo -n (set_color red)'# '
    end

    echo -n (set_color green) $hostname $prefix (set_color blue) (prompt_pwd) (__fish_git_prompt) (set_color red)'❯'(set_color yellow)'❯'(set_color green)'❯ '
    set_color normal

end

#!/bin/bash

wifi=$(cat /sys/class/net/wlp2s0/operstate)

if [ $wifi == "up" ]; then
    json=$(curl --connect-timeout 0.1 -s en.wttr.in/krakow?format=j1)
else
    exit 1
fi

if [ -n "$json" ]; then
    real=$(echo $json | jq '.current_condition[0].temp_C | tonumber')
    feels_like=$(echo $json | jq '.current_condition[0].FeelsLikeC | tonumber')

    uvi_idx=$(echo $json | jq '.current_condition[0].uvIndex | tonumber')

    weather_desc=$(echo $json | jq '.current_condition[0].weatherDesc[0].value')

    emoji_temp=""
    emoji_uvi_idx=""
    emoji_weather_desc=""

    if [ $feels_like -lt 0 ]; then
        emoji_temp="😢🥶"
    elif [ $feels_like -lt 5 ]; then
        emoji_temp="🥶"
    elif [ "$feels_like" -gt 30 ]; then
        emoji_temp="🥵"
    else
        emoji_temp="🥰"
    fi

    case $weather_desc in
        *Fog*|*fog*)
            emoji_weather_desc="F"
            ;;
        *snow*|*Snow*)
            emoji_weather_desc="❄️"
            ;;
        *Rain*|*rain*)
            emoji_weather_desc="🌨"
            ;;
        *Cloudy*|*cloudy*)
            emoji_weather_desc="⛅️"
            ;;
        *VeryCloudy*)
            emoji_weather_desc="☁️"
            ;;
        *Thunder*|*thunder*)
            emoji_weather_desc="🌩"
            ;;
        *Sunny*|*sunny*)
            emoji_weather_desc="☀️"
            ;;
        *)
            emoji_weather_desc=""
    esac

    output=$feels_like
    output+="°C|"
    output+="UVI."
    output+=$uvi_idx
    output+=""
fi

echo -e "{\"text\": \"$output\"}"

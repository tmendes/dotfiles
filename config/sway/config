#xwayland disable

# VARIABBLES
set {
    $mod Mod4
    $alt Alt
    $ctrl Ctrl
    $mybindsym bindsym --to-code

    # BASIC COMMANDS
    $term alacritty
    $lock swaylock
    $lock_pause playerctrl pause
    $notify notify-send

    # MENU COMMANDS
    $menu wofi

    # SCREENSHOT
    $screenshot_slurp grim -g "$(slurp)" $(xdg-user-dir PICTURES)/Screenshots/sway-$(date -u +\"%Y-%m-%d-%H%M%S\").png | wl-copy
    $screenshot_full grim $(xdg-user-dir PICTURES)/Screenshots/sway-$(date -u +\"%Y-%m-%d-%H%M%S\").png | wl-copy

    # SCREEN RECORDER
    $screen_recorder wf-recorder -f $(xdg-user-dir VIDEOS)/sway-$(date -u +\"%Y-%m-%d-%H%M%S\").mp4

    # AIRPLANE MODE
    $airplane_on nmcli radio all off
    $airplane_off nmcli radio all on

    # VOLUME
    $volume_up exec --no-startup-id wpctl set-volume @DEFAULT_SINK@ 5%+
    $volume_down exec --no-startup-id wpctl set-volume @DEFAULT_SINK@ 5%-
    $volume_mute exec --no-startup-id wpctl set-mute @DEFAULT_SINK@ toggle

    # SCREEN BRIGHT
    $bright_up exec --no-startup-id light -A 5
    $bright_down exec --no-startup-id light -U 5

    # VARIABBLES FOR DPMS CONTROL
    $screen_brt_powersave light -U 80%
    $screen_bri_max light -A 100%
    $screen_off swaymsg "output * dpms off"
    $screen_on swaymsg "output * dpms on"

    # OUTPUTS
    $output_main eDP-1
    $output_external HDMI-A-1
    $laptop $output_main

    # ARROWS CONFIG
    $left Left
    $down Down
    $up Up
    $right Right

    # WORKSPACE ICONS
    $WS1 1
    $WS2 2
    $WS3 3
    $WS4 4
    $WS5 5
    $WS6 6
    $WS7 7
    $WS8 8
    $WS9 9✻

    # NOT USED
    ###########################################################################
    # KEYBOARD LAYOUT
    $menupaste clipman pick --tool wofi -T'--prompt=It is all there -i'
    $layout_en exec swaymsg 'input * xkb_layout us(alt-intl)' && $notify English
    $layout_pl exec swaymsg 'input * xkb_layout pl' && $notify Polish
    ###########################################################################
}

# MESSAGES/STRINGS
set {
    # MODES MESSAGES
    $mode_airplane 'Airplane mode: (o)n o(f)f'
    $mode_record '(f)ullscreen (s)elected (v)ideo'
    $mode_resize '(arrows) to resize'
    $mode_power '(r)eload (e)xit reb(o)ot (h)ibernate (p)oweroff'

    # AIRPLANE MESSAGES
    $airplane_mode_on 'Airplane Mode On'
    $airplane_mode_off 'Airplane Mode Off'

    # QUIT SWAY MESSAGES
    $power_exit_msg '[EXIT] Are you sure?'
    $power_exit_confirm 'Yes. But I will be back soon'
    $power_exit_cmd 'swaymsg exit'

    # REBOOT MESSAGES
    $power_reboot_msg '[REBOOT] Are you sure?'
    $power_reboot_confirm 'Yes! Do it!'
    $power_reboot_cmd 'reboot'

    # HIBERNATE MESSAGES
    $power_hibernate_msg '[HIBERNATE] Are you sure?'
    $power_hibernate_confirm  'Yes. I am feeling so tired'
    $power_hibernate_cmd 'hibernate'

    # POWEROFF MESSAGES
    $power_poweroff_msg '[POWEROFF] Are you sure?'
    $power_poweroff_confirm 'I am damn sure!'
    $power_poweroff_cmd 'poweroff'
}

# FONT CONFIG
font DejaVu Sans 12, pango:FontAwesome 12

# COLORS                 BORDER    BACKGROUND  TEXT     INDICATOR  CHILD_BORDER
client.focused          #6272A4 #6272A4 #F8F8F2 #6272A4   #FFA500
client.focused_inactive #44475A #44475A #F8F8F2 #44475A   #44475A
client.unfocused        #282A36 #282A36 #BFBFBF #282A36   #282A36
client.urgent           #44475A #FF5555 #F8F8F2 #FF5555   #FF5555
client.placeholder      #282A36 #282A36 #F8F8F2 #282A36   #282A36
client.background       #F8F8F2

# GAPS CONFIG
smart_gaps on
gaps inner 1
gaps outer 1

# FOCUS CONFIG
focus_follows_mouse no
focus_wrapping no
mouse_warping output
popup_during_fullscreen smart

# WINDOW BORDER CONFIG
smart_borders on
hide_edge_borders smart
default_border pixel 2
default_floating_border normal 2

# WORKSPACE CONFIG
workspace_auto_back_and_forth yes

# SEAT
seat {
    seat0 hide_cursor 5000
    seat1 hide_cursor 5000
}

# LID
bindswitch {
    --reload --locked lid:on output $laptop disable
    --reload --locked lid:off output $laptop enable
}

# OUTPUTS
output {
    $output_main {
        scale 2
    }
    "*" bg $HOME/Pictures/wallpapers/flowers4.jpg fill

}

# INPUTS
input {
    # TOUCHSCREEN
    #1267:8400:ELAN_Touchscreen" {
    #    events disabled
    #}

    # TOUCHPAD
    "1739:30381:DLL0665:01_06CB:76AD_Touchpad" {
        tap enabled
        drag enabled
        drag_lock enabled
        middle_emulation enabled
        dwt enabled
        events disabled_on_external_mouse
        pointer_accel 1
        natural_scroll enable
    }

    # KEYBOARD
    "1:1:AT_Translated_Set_2_keyboard" {
        repeat_rate 150
        repeat_delay 300
        xkb_capslock disabled
        xkb_numlock disable
        xkb_layout us(alt-intl),pl
        xkb_options grp:win_space_toggle
    }
}

# KEY BINDING
$mybindsym {
    # MAIN KEY BINDINGS
    $mod+Return exec $term
    $mod+d exec $menu
    $mod+l exec --no-startup-id $lock
    $mod+q kill

    # KEY BINDINGS - MOVE FOCUS AROUND
    $mod+$left focus left
    $mod+$down focus down
    $mod+$up focus up
    $mod+$right focus right

    # MOVE WINDOW
    $mod+z+$up move up
    $mod+z+$down move down
    $mod+z+$left move left
    $mod+z+$right move right

    $mod+Shift+n exec swaync-client -t -sw

    # KEY BINDINGS - MOVE WORKSPACE BTW DISPLAYS
    $mod+m+$down move workspace to output $output_main
    $mod+m+$up move workspace to output $output_external

    # KEY BINDIGS - MOVE CONTAINERS TO WORKSPACES
    $mod+m+1 move container to workspace $WS1
    $mod+m+2 move container to workspace $WS2
    $mod+m+3 move container to workspace $WS3
    $mod+m+4 move container to workspace $WS4
    $mod+m+5 move container to workspace $WS5
    $mod+m+6 move container to workspace $WS6
    $mod+m+7 move container to workspace $WS7
    $mod+m+8 move container to workspace $WS8
    $mod+m+9 move container to workspace $WS9
    $mod+m+0 move container to workspace $WS0

    # KEY BINDINGS - SWITCH TO WORKSPACE
    $mod+1 workspace $WS1
    $mod+2 workspace $WS2
    $mod+3 workspace $WS3
    $mod+4 workspace $WS4
    $mod+5 workspace $WS5
    $mod+6 workspace $WS6
    $mod+7 workspace $WS7
    $mod+8 workspace $WS8
    $mod+9 workspace $WS9

    # KEY BINDINGS - CONTAINER LAYOUT
    $mod+h splith
    $mod+v splitv
    $mod+f fullscreen
    $mod+t layout tabbed
    $mod+s layout toggle split

    # KEY BINDINGS - FLOAT MODE TOGGLE
    $mod+space floating toggle

    --locked XF86MonBrightnessDown $bright_down
    --locked XF86MonBrightnessUp $bright_up
    --locked XF86AudioRaiseVolume $volume_up
    --locked XF86AudioLowerVolume $volume_down
    --locked XF86AudioMute $volume_mute
}

# MODES
mode {
    # SCREENSHOT/VIDEO PANEL KEY BINDINGS
    $mode_record {
        $mybindsym {
            f exec --no-startup-id $screenshot_full, mode "default"
            s exec --no-startup-id $screenshot_slurp, mode "default"
            v exec --no-startup-id $screen_recorder, mode "default"
            Return mode "default"
        }
    }

    # AIRPLANE MODE
    $mode_airplane {
        $mybindsym {
            o exec --no-startup-id $airplane_on && $notify $airplane_mode_on, mode "default"
            f exec --no-startup-id $airplane_off && $notify $airplane_mode_off, mode "default"
            Return mode "default"
        }
    }

    # RESIZE PANEL KEY BINDINGS
    $mode_resize {
        $mybindsym {
            $left resize shrink width 10px
            $down resize grow height 10px
            $up resize shrink height 10px
            $right resize grow width 10px
            Return mode "default"
        }
    }

    $mode_power {
        $mybindsym {
            r exec swaymsg reload, mode "default"
            e exec swaynag -m $power_exit_msg -b $power_exit_confirm $power_exit_cmd
            o exec swaynag -m $power_reboot_msg -b $power_reboot_confirm $power_reboot_cmd
            h exec swaynag -m $power_hibernate_msg -b $power_hibernate_confirm $power_hibernate_cmd
            p exec swaynag -m $power_poweroff_msg -b $power_poweroff_confirm $power_poweroff_cmd
            Return mode "default"
        }
    }
}

# EXTRA BIND MODES
$mybindsym {
    $mod+Print mode $mode_record
    Print mode $mode_airplane
    $mod+r mode $mode_resize
    $mod+p mode $mode_power
}

bar {
    swaybar_command waybar
    workspace_buttons yes
	position top
	height 25
    colors {
        statusline #ffffff
        background #323232
        inactive_workspace #32323200 #32323200 #5c5c5c
    }
}

# SWAY
exec {
    systemctl --user import-environment
    systemctl --user import-environment DISPLAY WAYLAND_DISPLAY SWAYSOCK XDG_CURRENT_DESKTOP
    wl-paste -t text --watch clipman store --no-persist
    swayidle -w \
        timeout 300 '$screen_brt_powersave' \
        resume '$screen_bri_max' \
        timeout 1800 '$lock' \
        timeout 1900 '$screen_off' \
        resume '$screen_on' \
        before-sleep '$lock_pause' \
        before-sleep '$lock'
}

exec_always {
    /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
    gsettings set org.gnome.desktop.interface font-name 'DejaVu Sans 12'
    pactl load-module module-switch-on-connect
}

# APPS TO START
exec {
    mullvad-vpn
    #mako
    swaync
    gammastep-indicator
    udiskie -an --appindicator
    wl-paste -t text --watch clipman store --no-persist
}

# WINDOW CONFIG
for_window
{
    ## Inhibit Idle if a window is fullscreen
    [class="^.*"                        ] inhibit_idle fullscreen
    [app_id="^.*"                       ] inhibit_idle fullscreen

    ## Inhibit Idle for some apps
    [class=".*mpv.*"                    ] inhibit_idle visible
    [app_id=".*mpv.*"                   ] inhibit_idle visible
    [class=".*vlc.*"                    ] inhibit_idle visible
    [class=".*reeTube.*"                ] inhibit_idle visible

    ## Floating for some alerts
    [app_id="blueberry.py"              ] floating enable
    [app_id="blueman-manager"           ] floating enable
    [app_id="pavucontrol"               ] floating enable
    [class=".*Tor.*"                    ] floating enable
    [app_id=".*Tor.*"                   ] floating enable
    [title="^Android Emulator.*"        ] floating enable

    [title="Picture-in-Picture.*"       ] floating enable
    [title="Save File.*"                ] floating enable
    [title="Open File.*"                ] floating enable
    [title=".* is sharing your screen.*"] floating enable
    [title=".*Sharing Indicator.*      "] floating enable, move scratchpad
}

# APP WORKSPACE
assign {
    ## ALL/OTHERS
    [class=".*hromium.*"     ] $WS1
    [class=".*oplin.*"       ] $WS1
    [app_id=".*.nki"         ] $WS1

    ## BROWSWER
    [class=".*Tor.*"         ] $WS2
    [class=".*irefox.*"      ] $WS2
    [app_id=".*irefox.*"     ] $WS2

    ## CODE
    [class=".*etbrains.*"    ] $WS3
    [class="code-oss.*"      ] $WS3
    [class=".*.odium"       ] $WS3
    [class=".*ndroid.*"      ] $WS4

    ## TOOLS
    [class="Postman"         ] $WS5
    [app_id="libreoffice-*"  ] $WS5
    [class=".*eePassXC.*"    ] $WS5
    [app_id=".*eePassXC.*"   ] $WS5
    [app_id="wireshark.*"    ] $WS5
    [app_id=".*remmina.*"    ] $WS5
    [class=".*iking.*"       ] $WS5

    ## MEDIA
    [class="vlc.*"           ] $WS6
    [class=".*imp.*"         ] $WS6
    [class=".*udacity.*"     ] $WS6
    [app_id="mpv.*"          ] $WS6
    [class=".*denlive.*"     ] $WS6
    [class=".*reeTube.*"     ] $WS6
    [class=".*potify.*"      ] $WS6
    [class=".*useScore.*"    ] $WS6
    [class=".*potify.*"      ] $ws6
    [app_id=".*.etronome"    ] $WS6

    ## SIGNAL
    [app_id=".*ignal.*"      ] $WS7
    [class=".*ignal.*"       ] $WS7

    ## EMAIL
    [class=".*utanota.*"     ] $WS8

    ## RANDOM
    [app_id=".*ransmission.*"] $WS9
    [app_id=".*yncthing.*"   ] $WS9
}


# IMPORT SWAY BASIC CONFIG
include /etc/sway/config.d/*

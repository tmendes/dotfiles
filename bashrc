### PS1 CONFIG

ICONS=( 🧟 🚴 👿 👻 🐒 🐃 🐄 🦖 🐧 🦃 🐓 🐡 )
IDX=$(( ( RANDOM % 12 )))
PS1_BODY="\[$(tput setaf 5)\]\h \[$(tput bold)\]\[$(tput setaf 2)\]\W\[$(tput setaf 4)\]\[$(tput sgr0)\]"
PS1_PREFIX="${ICONS[$IDX]}💨"

### EXPORTS

# PS1
export PS1="$PS1_PREFIX $PS1_BODY "

# CEDILHA
export GTK_IM_MODULE=cedilla
export QT_IM_MODULE=cedilla

# TERM
export TERM=xterm-256color

# OTHERS
export PAGER=more
export EDITOR=nvim

# JDK
STUDIO_JDK=/usr/lib/jvm/java-8-openjdk

# PATHS

SCRIPTS_PATH=$HOME/Documents/devel/projects/scripts

[ -d "$SCRIPTS_PATH" ] && PATH=$SCRIPTS_PATH:${PATH}

HOME_BIN=$HOME/.usr/bin
[ -d $HOME_BIn ] && PATH=$HOME_BIN:${PATH}

ANDROID_BIN_PATH=$ANDROID_HOME/tools:$ANDROID_HOME/plattform-tools
[ -d "$ANDROID_BIN_PATH" ] && PATH=$ANDROID_BIN_PATH:${PATH}

CARGO_BIN_PATH=$HOME/.cargo/bin/
[ -d "$CARGO_BIN_PATH" ] && PATH=$CARGO_BIN_PATH:${PATH}

YARN_BIN_PATH=$HOME/.yarn/bin
[ -d "$YARN_BIN_PATH" ] && PATH=$YARN_BIN_PATH:${PATH}

NPM_G_BIN_PATH=$HOME/.npm-global/bin
[ -d "$NPM_G_BIN_PATH" ] && PATH=$NPM_G_BIN_PATH:${PATH}

export GOPATH=$HOME/Documents/devel/projects/golang-workspace
[ -d "$GOPATH" ] && PATH=$GOPATH/bin:${PATH}

export PATH

export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/local/lib/pkgconfig

### ALIAS

#alias sway="cd ~ && sway"
alias sway="cd ~ && exec dbus-launch --sh-syntax --exit-with-session sway"
alias virtualbox="env QT_QPA_PLATFORM=xcb virtualbox"
alias open='xdg-open'
alias ls='exa'
alias tree='exa --tree'
alias cal='cal -3 --color=always -m'
alias vim='nvim'

alias maintenance_lostfiles='sudo lostfiles'
alias maintenance_foreign='yay -Qm'
alias maintenance_failed='systemctl --failed'
alias maintenance_errors='journalctl -p 3 -xb'
alias maintenance_orphans='sudo pacman -Rns $(pacman -Qtdq)'
alias maintenance_broken_links='sudo find / -xtype l -print'
alias maintenance_clean_deps='yay -Yc'
alias maintenance_last_30="expac --timefmt='%Y-%m-%d %T' '%l\t%n' | sort | tail -n 30"
alias maintenance_stats="yay -P --stats"
alias maintenance_services="systemctl list-units --type=service"
alias maintenance_pkg_history="expac --timefmt='%Y-%m-%d %T' '%l\t%n'|sort -n"

alias _projects="cd ~/Documents/devel/projects"
alias _go="cd ~/Documents/devel/projects/golang-workspace/src"
alias _rust="cd ~/Documents/devel/projects/rust-workspace"
alias _react="cd ~/Documents/devel/projects/react-workspace"
alias _opensource="cd ~/Documents/devel/opensource"

### HISTORY

shopt -s histappend
export HISTCONTROL=ignorespace

### PLUGINS

# BASH_GIT_PROMPT
GIT_PROMPT_ONLY_IN_REPO=1
GIT_PROMPT_FETCH_REMOTE_STATUS=0
GIT_PROMPT_SHOW_UPSTREAM=0

source ~/.bash-git-prompt/gitprompt.sh

function maintenance_clean() {
    #echo "Clean Pacman Cache - Safe mode"
    #sudo pacman -rk 1
    echo "Clean Pacman Cache"
    sudo pacman -Sc
    echo "Clean Systemd Temp Files"
    sudo systemd-tmpfiles --clean
}
